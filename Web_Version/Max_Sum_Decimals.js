//===============================
// Main Algorithm
//===============================

//Main function to execute
function Money_Knapsack_Execute(max_sum_str, possible_items)
{
    //Convert each input item to a double and add to array of values and weights (which are the same)
    //Note that we multiply by 100 to convert decimal cents to be INTEGERS
    var values = Array(possible_items.length);
    var weights = Array(possible_items.length);
    for (var i = 0; i < possible_items.length; ++i) {
        values[i]  = Math.floor(100 * parseFloat(possible_items[i]));
        weights[i] = values[i];
    }
    
    //Get the max value
    //Note that we multiply by 100 to convert decimal cents to an INTEGER
    var max_val = Math.floor(100 * parseFloat(max_sum_str));

    //Execute the knapscak algorithm to find the objects that sum to below the max_val
    var result = knapsack(max_val, weights, values, possible_items.length);
    
    //Convert back from INTEGERS to the decimal values
    var used_values = ""; //Which items were used to get to the sum
    var total_value = 0;  //The total sum of the items used
    for (var i = 0; i < result.length; ++i) {
        var the_value = ((result[i]) / 100.0);       //Convert to the decimal value
        used_values += the_value + "\n"; //List the used items
        total_value += the_value;                         //Determine the total sum of the items used
    }
    
    return [used_values, total_value];
}

//The main knapsack algorithm
function knapsack(max_value, weights, values, n)
{
    //This program does not take credit for the algorithm. This program was largely used
    //to get a very basic user interface around the knapsack algorithm.
    //This version of the knapsack was taken from:
    //https://www.geeksforgeeks.org/printing-items-01-knapsack/

    var i = 0;
    var w = 0;
    var K = [];
    for (i = 0; i < n + 1; ++i) {
        K.push(Array(max_value+1));
    }

    //Build table K[][] in bottom up manner
    for (i = 0; i <= n; i++)
    {
        for (w = 0; w <= max_value; w++)
        {
            if (i == 0 || w == 0) {
                K[i][w] = 0;
            }
            else if (weights[i - 1] <= w) {
                K[i][w] = Math.max(values[i - 1] + K[i - 1][w - weights[i - 1]], K[i - 1][w]);
            }
            else {
                K[i][w] = K[i - 1][w];
            }
        }
    }

    //Stores the result of Knapsack
    var res = K[n][max_value];

    //Below we will determine which objects were used in the knapsack
    var knapsack_items = [];

    w = max_value;
    for (i = n; i > 0 && res > 0; i--)
    {
        //Either the result comes from the top (K[i-1][w]) or from (val[i-1] + K[i-1][w-weights[i-1]])
        //as in Knapsack table. If it comes from the latter one/ it means the item is included.
        if (res == K[i-1][w]) {
            continue;
        }
        else
        {
            //This item is included.
            knapsack_items.push(weights[i - 1]);

            //Since this weight is included its value is deducted
            res = res - values[i - 1];
            w = w - weights[i - 1];
        }
    }

    return knapsack_items;
}



//===============================
// Main Operator Action
//===============================

function Find_Max_Sum_Click()
{
    //Get the textboxes
    var max_sum_tbox = document.getElementById('max_sum_tbox');
    var possible_values_tbox = document.getElementById('possible_values_tbox');
    var result_sum_tbox = document.getElementById('result_sum_tbox');
    var resulting_values_tbox = document.getElementById('resulting_values_tbox');
    
    //Clear any errors
    max_sum_tbox.style.backgroundColor = "White";
    possible_values_tbox.style.backgroundColor = "White";

    //Get the values from the textbox. Make sure to remove all spaces
    var values = possible_values_tbox.value.split(/[\s]+/);
    for (var i = 0; i < values.length; ++i) {
        values[i] = Format_Entry(values[i].trim());
    }

    //Remove all whitespace items
    while(values.includes("")) {
        const index = values.indexOf("");
        values.splice(index, 1); 
    }

    //Output the modified values to the display
    if (possible_values_tbox.tag != "GHOST") {
        var new_text = "";
        for (var i = 0; i < values.length; ++i) {
            new_text += values[i] + "\n";
        }
        possible_values_tbox.value = new_text;
    }

    try {
        //Output the result to the display
        var result = Money_Knapsack_Execute(max_sum_tbox.value.trim(), values);
        resulting_values_tbox.value = result[0];
        result_sum_tbox.value = result[1];
    }
    catch (exc)
    {
        resulting_values_tbox.value = "Exception Thrown!\n";
        resulting_values_tbox.value += "\n\nNOTE: The error may be in the Possible Values field or the Max Sum field";
        result_sum_tbox.value = "N/A";

        //Highlight the potential errors
        max_sum_tbox.style.backgroundColor         = "Pink";
        possible_values_tbox.style.backgroundColor = "Pink";
    }
}

//Converts a possible value entry to be just a number
function Format_Entry(value)
{
    //List of acceptable characters
    var acceptable_chars = "0123456789.";

    //Create a return value
    var return_value = "";

    //Go through each char in the original
    for (var i = 0; i < value.length; ++i)
    {
        //If it is an acceptable char, add it to the output value
        if (acceptable_chars.includes(value[i])) {
            return_value += value[i];
        }
    }
    return return_value;
}

//===============================
// Ghost Text
//===============================

function Lost_Focus(tbox_name) 
{
    //Get the element from its name
    var elem = document.getElementById(tbox_name);
    
    //If there is no text in the textbox when the user clicks away, then add ghost text
    if (elem.value.trim() == "") 
    {
        var new_text = "";
        new_text += "Enter values here. Formats are:\n";
        new_text += "- 1 Per Line\n";
        new_text += "- Space Delimited\n";
        new_text += "- Tab Delimited\n\n";
        new_text += "Then enter a max sum and click 'Find Max Sum'\n\n";
        new_text += "Notes:\n";
        new_text += "- Commas are NOT a delimiter\n";
        new_text += "- Commas removed to avoid issues with thousands\n";
        new_text += "- All non-numeric values will be removed";
        
        //Set the ghost text
        elem.value = new_text;
        
        //Set the text color
        elem.style.color = "Gray";
        
        //Set the 
        elem.tag = "GHOST";
    }
}

function Got_Focus(tbox_name)
{
    //Get the element from its name
    var elem = document.getElementById(tbox_name);
    
    //If we were previously displaying ghost text, then hide the ghost text
    if (elem.tag == "GHOST") {
        elem.tag = "";
        elem.value = "";
        elem.style.color = "Black";
    }
}

//===============================
// On Initialization
//===============================

//Things that occur as soon as the javascript loads
Lost_Focus("possible_values_tbox");
    