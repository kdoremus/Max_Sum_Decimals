﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Find_Max_Sum_Below_Value
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow() {
            InitializeComponent();

            //Display the ghost text instructions
            Lost_Focus(possible_values_tbox);
        }

        private void Find_Max_Sum_Click(object sender, RoutedEventArgs e)
        {
            //Clear any errors
            max_sum_tbox.Background         = new SolidColorBrush(Colors.White);
            possible_values_tbox.Background = new SolidColorBrush(Colors.White);

            //Get the values from the textbox. Make sure to remove all spaces
            List<string> values = possible_values_tbox.Text.Trim().Split('\n', '\t', ',', '\r', ' ').ToList();
            for (int i = 0; i < values.Count; ++i) {
                values[i] = Format_Entry(values[i].Trim());
            }

            //Remove all whitespace items
            while(values.Contains("")) {
                values.Remove("");
            }

            //Output the modified values to the display
            if (possible_values_tbox.Tag.ToString() != "GHOST") {
                possible_values_tbox.Text = String.Join("\n", values);
            }

            try {
                //Output the result to the display
                Tuple<string, double> result = Money_Knapsack.Execute(max_sum_tbox.Text.Trim(), values);
                resulting_values_tbox.Text = result.Item1;
                resulting_value_tbox.Text = result.Item2.ToString("#.##");
            }
            catch (Exception exc)
            {
                resulting_values_tbox.Text = "Exception:\n" + exc.Message;
                resulting_values_tbox.Text += "\n\nNOTE: The error may be in the Possible Values field or the Max Sum field";
                resulting_value_tbox.Text = "N/A";

                //Highlight the potential errors
                max_sum_tbox.Background         = new SolidColorBrush(Colors.Pink);
                possible_values_tbox.Background = new SolidColorBrush(Colors.Pink);
            }
        }

        //Converts a possible value entry to be just a number
        private string Format_Entry(string value)
        {
            //List of acceptable characters
            const string acceptable_chars = "0123456789.";

            //Create a return value
            string return_value = "";

            //Go through each char in the origianl
            foreach(char c in value)
            {
                //If it is an acceptable char, add it to the output value
                if (acceptable_chars.Contains(c)) {
                    return_value += c;
                }
            }

            return return_value;
        }

        
        void Lost_Focus(TextBox tbox)
        {
            //If there is no text in the textbox when the user clicks away, then add ghost text
            if (tbox.Text.Trim() == "") {
                tbox.Tag = "GHOST";
                tbox.Foreground = new SolidColorBrush(Colors.Gray);
                tbox.Text  = "Enter values here. Formats are:\n";
                tbox.Text += "- 1 Per Line\n";
                tbox.Text += "- Space Delimited\n";
                tbox.Text += "- Tab Delimited\n\n";
                tbox.Text += "Then enter a max sum and click 'Find Max Sum'\n\n";
                tbox.Text += "Notes:\n";
                tbox.Text += "- Commas are NOT a delimiter\n";
                tbox.Text += "- Commas removed to avoid issues with thousands\n";
                tbox.Text += "- All non-numeric values will be removed";
            }
        }

        void Got_Focus(TextBox tbox)
        {
            //If we were previously displaying ghost text, then hide the ghost text
            if (tbox.Tag.ToString() == "GHOST") {
                tbox.Tag = "";
                tbox.Text = "";
                tbox.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void Got_Foucs(object sender, RoutedEventArgs e) {
            Got_Focus((TextBox)sender);
        }

        private void Lost_Focus(object sender, RoutedEventArgs e) {
            Lost_Focus((TextBox)sender);
        }
    }

    //==============================================================================
    // Knapsack algorithm
    //==============================================================================

    //Class to group the knapsack algorith function calls
    //We execute our knapsack on money (dollars) which is important to note so that
    //we can convert fractional dollars to integers by multiplying to get past the cents fields.
    public class Money_Knapsack
    {
        //Main function to execute
        public static Tuple<string, double> Execute(string max_value, List<string> possible_items)
        {
            //Convert each input item to a double and add to array of values and weights (which are the same)
            //Note that we multiply by 100 to convert decimal cents to be INTEGERS
            int[] values = new int[possible_items.Count];
            int[] weights = new int[possible_items.Count];
            for (int i = 0; i < possible_items.Count; ++i) {
                values[i]  = (int)Math.Floor(Double.Parse(possible_items[i]) * 100);
                weights[i] = values[i];
            }

            //Get the max value
            //Note that we multiply by 100 to convert decimal cents to an INTEGER
            int max_val = (int)Math.Floor(Double.Parse(max_value) * 100);

            //Execute the knapscak algorithm to find the objects that sum to below the max_val
            List<int> result = knapsack(max_val, weights, values, possible_items.Count);

            //Convert back from INTEGERS to the decimal values
            string used_values = ""; //Which items were used to get to the sum
            double total_value = 0;  //The total sum of the items used
            foreach(int val in result) {
                double the_value = (((double)val) / 100.0);       //Convert to the decimal value
                used_values += the_value.ToString("#.##") + "\n"; //List the used items
                total_value += the_value;                         //Determine the total sum of the items used
            }

            return new Tuple<string, double>(used_values, total_value);
        }

        //The main knapsack algorithm
        private static List<int> knapsack(int max_value, int[] weights, int[] values, int n)
        {
            //This program does not take credit for the algorithm. This program was largely used
            //to get a very basic user interface around the knapsack algorithm.
            //This version of the knapsack was taken from:
            //https://www.geeksforgeeks.org/printing-items-01-knapsack/

            int i, w;
            int[,] K = new int[n + 1, max_value + 1];

            //Build table K[][] in bottom up manner
            for (i = 0; i <= n; i++)
            {
                for (w = 0; w <= max_value; w++)
                {
                    if (i == 0 || w == 0) {
                        K[i, w] = 0;
                    }
                    else if (weights[i - 1] <= w) {
                        K[i, w] = Math.Max(values[i - 1] + K[i - 1, w - weights[i - 1]], K[i - 1, w]);
                    }
                    else {
                        K[i, w] = K[i - 1, w];
                    }
                }
            }

            //Stores the result of Knapsack
            int res = K[n, max_value];

            //Below we will determine which objects were used in the knapsack
            List<int> knapsack_items = new List<int>();

            w = max_value;
            for (i = n; i > 0 && res > 0; i--)
            {
                //Either the result comes from the top (K[i-1][w]) or from (val[i-1] + K[i-1][w-weights[i-1]])
                //as in Knapsack table. If it comes from the latter one/ it means the item is included.
                if (res == K[i - 1, w]) {
                    continue;
                }
                else
                {
                    //This item is included.
                    knapsack_items.Add(weights[i - 1]);

                    //Since this weight is included its value is deducted
                    res = res - values[i - 1];
                    w = w - weights[i - 1];
                }
            }

            return knapsack_items;
        }
    }
}
